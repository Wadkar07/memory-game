let firstFlipped = false;
let firstCardClassList;
let secondCardClassList;
let score = 0;
let flip = 0;
let pairSelected = true;

let arr = Array.from({ length: 12 }, (_, i) => `${i + 1}.gif`);
let gifsArray = [...shuffle(arr), ...shuffle(arr)];

function start() {
    score = 0;
    gifsArray = shuffle(gifsArray);

    document.getElementById('game').innerText = '';

    let scoreDiv = document.createElement('div');
    scoreDiv.className = 'score-div';

    let scorePoint = document.createElement('p');
    scorePoint.className = 'score-card';
    scorePoint.id = `currentScore`;

    let lastBestScore = localStorage.getItem('Best Score');
    let bestScore = document.createElement('p');
    bestScore.className = 'score-card';
    bestScore.id = `best-score`;

    if (lastBestScore) {
        bestScore.innerText = `Best Score : ${lastBestScore}`
    }
    else {
        bestScore.innerText = `First one to play this challenge`
    }

    scoreDiv.append(scorePoint,bestScore);

    let clicks = document.createElement('p');
    clicks.innerText = 'Current Score : 0';
    scorePoint.appendChild(clicks);

    let cards = generateLevel(gifsArray);

    let restartDiv = document.createElement('div');
    restartDiv.id = 'restartDiv';


    let buttonDiv = document.createElement('div');
    buttonDiv.className = 'btn-div';

    let restartButton = document.createElement('a');
    restartButton.innerText = "restart Game";
    restartButton.id = 'restart';
    restartButton.addEventListener('click', () => {
        restart('restart');
    }, false);

    let exitButton = document.createElement('a');
    exitButton.innerText = "Exit Game";
    exitButton.id = 'exit';
    exitButton.addEventListener('click', () => {
        restart('exit');
    }, false);

    restartDiv.appendChild(restartButton);

    buttonDiv.append(restartDiv,exitButton)

    // document.getElementById('game').appendChild(bestScore);
    document.getElementById('game').appendChild(scoreDiv);
    document.getElementById('game').appendChild(...cards);
    document.getElementById('game').appendChild(buttonDiv);
}

function shuffle(array) {
    let currentIndex = array.length, randomIndex;
    while (currentIndex != 0) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;
        [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]];
    }
    return array;
}

function generateLevel(gifsArray) {
    const gifCards = document.createElement('div')
    gifCards.id = 'gif-cards';

    const cards = gifsArray.map((gifNo) => {

        let card = document.createElement('div');

        let front = document.createElement('img');
        front.className = "front"
        front.src = `./gifs/front.png`;

        let back = document.createElement('img');
        back.className = "back";
        back.src = `./gifs/${gifNo}`;

        card.appendChild(front);
        card.appendChild(back);
        card.addEventListener('click', cardFlipped, false);
        card.className = `card ${gifNo}`;

        gifCards.appendChild(card);
        return gifCards;
    });
    return cards;

}

function cardFlipped() {
    if (pairSelected === false || this.classList.contains('flip')
    ) {
        return;
    }
    else {
        document.getElementById('currentScore').color = 'black';
        if (!(this.classList.contains('flip'))) {
            document.getElementById('currentScore').innerText = `Current Score : ${++score}`;
            this.classList.add('flip');
        }

        if (firstFlipped === false) {
            firstCardClassList = this;
            firstFlipped = true;
        }
        else {
            secondCardClassList = this;
            firstFlipped = false;
            pairSelected = false;
            checkCards(firstCardClassList, secondCardClassList);
        }
    }
}

function checkCards(first, second) {
    if (first.classList[1] !== second.classList[1]) {
        setTimeout(() => {
            first.classList.remove('flip');
            second.classList.remove('flip');
            pairSelected = true;
            return false;
        }, 1000);
    }
    else {
        pairSelected = true;
        arr = arr.filter((gif) => {
            return gif !== first.classList[1];
        })
        if (arr.length === 0) {
            winnerPrompt();
        }
        else {
            return false;
        }
    }
}

function winnerPrompt() {
    document.getElementById('game').innerText = "";

    const winMessage = document.createElement('h1');
    winMessage.innerText = "Hurray! you won";

    let winnerPromptDiv = document.createElement('div');
    winnerPromptDiv.id = "winner-prompt-div";

    document.getElementById('game').appendChild(winMessage);

    let playAgainButton = document.createElement('a');
    playAgainButton.innerText = "Replay Game";
    playAgainButton.href = "./index.html";

    let playerScore = document.createElement('h3');
    const localBestScore = localStorage.getItem('Best Score');

    if (localBestScore) {
        let previousBestScore = document.createElement('h3');

        if (localStorage.getItem('Best Score') > score) {
            localStorage.setItem('Best Score', score);
            playerScore.innerText = `You made the new best score of ${score} flips`;
            previousBestScore.innerText = `Previous Best Score : ${localStorage.getItem('Best Score')}`;
        }

        else {
            playerScore.innerText = `Your score : ${score} flips`;
            previousBestScore.innerText = `Best Score : ${localStorage.getItem('Best Score')} flips`;
        }

        winnerPromptDiv.appendChild(playerScore);
        winnerPromptDiv.appendChild(previousBestScore);
    }

    else {
        localStorage.setItem('Best Score', score);
        playerScore.innerText = `You made the new best score of ${score} flips`;
        document.getElementById('winner-prompt-div').appendChild(playerScore);
    }

    document.getElementById('game').appendChild(winnerPromptDiv);

    document.getElementById('game').appendChild(playAgainButton);
}


function restart(operation) {
    document.getElementById('exit').style.display = 'none';

    document.getElementById('restart').innerText = `want to ${operation} the game`;

    document.getElementById('gif-cards').style.display = 'none';
    let restartOptions = document.createElement('div');
    restartOptions.id = "restartOptions"


    let confirmRestart = document.createElement('a');
    confirmRestart.innerText = 'YES';
    confirmRestart.style.backgroundColor = "#f55b6a";

    if (operation === 'restart') {
        confirmRestart.addEventListener('click', () => {
            start();
        }, false);
    }
    else {
        confirmRestart.href = "./index.html";
    }

    let cancelRestart = document.createElement('a');
    cancelRestart.addEventListener('click', resetRestartMenu, false);
    cancelRestart.innerText = 'NO';
    cancelRestart.style.backgroundColor = "#5bf565";

    restartOptions.append(confirmRestart, cancelRestart)

    document.getElementById('restartDiv').append(restartOptions);

}


function resetRestartMenu() {
    document.getElementById('exit').style.display = 'block';
    document.getElementById('gif-cards').style.display = 'flex';
    document.getElementById('restartDiv').innerText = "";

    let restartButton = document.createElement('a');
    restartButton.innerText = "restart Game";
    restartButton.id = 'restart';
    restartButton.addEventListener('click', () => {
        restart('restart');
    }, false);

    document.getElementById('restartDiv').appendChild(restartButton);
}